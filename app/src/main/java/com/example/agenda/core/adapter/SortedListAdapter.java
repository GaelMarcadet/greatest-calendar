package com.example.agenda.core.adapter;

import androidx.annotation.NonNull;

import java.util.Collection;
import java.util.List;

/**
 * Sorted list adapter is a specialization of a list adapter.
 *
 * All item stored in this adapter are sorted.
 * It's why an item must implement {@code Comparable} interface, in order to be compared with
 * another item.
 *
 * @param <T> Item's type
 */
public abstract class SortedListAdapter<T extends Comparable<T>> extends ListAdapter<T> {


    public SortedListAdapter( List<T> initialList ) {
        super( initialList );
    }

    @Override
    public T set( int index, T element ) {
        // cannot set an element at a specified index in a sorted list
        throw new UnsupportedOperationException( "Cannot set an item at specific index in sorted list" );
    }

    @Override
    public void add( int index, T element ) {
        // cannot set an element at a specified index in a sorted list
        throw new UnsupportedOperationException( "Cannot set an item at specific index in sorted list" );
    }

    @Override
    public boolean add( T item ) {
        // search position to place an item
        int index = 0;
        for ( T t : this.items ) {
            if ( 0 < item.compareTo( t ) ) {
                index++;
            } else {
                break;
            }
        }

        // insert item at proper place and notify adapter for an insertion
        this.items.add( index, item );
        notifyItemInserted( index );

        return true;
    }

    @Override
    public boolean addAll( @NonNull Collection<? extends T> c ) {
        for ( T item : c ) {
            this.add( item );
        }
        return true;
    }

    @Override
    public boolean addAll( int index, @NonNull Collection<? extends T> c ) {
        throw new UnsupportedOperationException( "Cannot set items at specific index in sorted list" );
    }
}
