package com.example.agenda.core.network;


public enum ADECalendarDownloadStatus {
    IN_PROGRESS,
    POST_DOWNLOAD,
    FINISHED,
}
