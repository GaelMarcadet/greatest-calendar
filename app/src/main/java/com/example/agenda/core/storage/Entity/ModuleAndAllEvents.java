package com.example.agenda.core.storage.Entity;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.ArrayList;
import java.util.List;

public class ModuleAndAllEvents {

    public Module module;


    public List<Event> eventsContainer;

    public void addEvent(Event event ) {
        this.eventsContainer.add( event );
    }

    public void updateEvents( List<Event> eventsContainer ) {
        List<Event> newEvents = new ArrayList<>();
        for ( Event event : eventsContainer ) {
            if ( event.getSummary().equals( this.module.getSummary()) ) {
                newEvents.add( event );
            }
        }
        this.eventsContainer = newEvents;
    }
}
