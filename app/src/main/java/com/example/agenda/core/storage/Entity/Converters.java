package com.example.agenda.core.storage.Entity;


import android.util.Log;

import androidx.room.TypeConverter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Converters {

    @TypeConverter
    public static Date StringToDate(String date){
        try {
            return new SimpleDateFormat( ScholarYear.DATE_FORMAT, Locale.FRANCE).parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }

    @TypeConverter
    public static String DateToString(Date date){
        DateFormat dateFormat = new SimpleDateFormat(ScholarYear.DATE_FORMAT,Locale.FRANCE);
        try {
            return dateFormat.format(date);
        } catch ( NullPointerException e ){
            Log.e( "DATE_CONVERTER", "DateToString: Cannot format null date"   );
            return "ERROR";
        }
    }


}
