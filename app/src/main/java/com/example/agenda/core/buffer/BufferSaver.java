package com.example.agenda.core.buffer;

import android.os.AsyncTask;

import com.example.agenda.core.storage.Entity.Event;
import com.example.agenda.core.storage.Entity.Module;
import com.example.agenda.core.storage.Entity.ScholarYear;
import com.example.agenda.core.storage.Facade.StorageInterface;

/**
 * Save buffer in database.
 */
public class BufferSaver extends AsyncTask<Void, Void, Void> {

    private StorageInterface storage;
    private OnDatabaseInteraction onDatabaseInteraction;
    private Buffer buffer;

    public BufferSaver( StorageInterface storage ) {
        this.buffer = Buffer.getInstance();
        this.storage = storage;
    }

    public void setOnDatabaseInteraction( OnDatabaseInteraction onDatabaseInteraction ) {
        this.onDatabaseInteraction = onDatabaseInteraction;
    }

    @Override
    protected void onPreExecute() {
        if ( onDatabaseInteraction != null ) {
            onDatabaseInteraction.onStart();
        }
    }

    @Override
    protected Void doInBackground( Void... voids ) {
        // saving scholar years
        for ( ScholarYear scholarYear : buffer.getScholarYears() ) {
            if ( scholarYear.getId() == 0 ) {
                storage.insertScholarYear( scholarYear );
            } else {
                storage.updateScholarYear( scholarYear );
            }
        }

        // saving modules
        for ( Module module : buffer.getModules() ) {
            if ( module.getId() == 0 ) {
                storage.insertModule( module );
            } else {
                storage.updateModule( module );
            }
        }

        // saving events
        for ( Event event : buffer.getEvents() ) {
            if ( event.getId() == 0 ) {
                storage.insertEvent( event );
            } else {
                storage.updateEvent( event );
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute( Void aVoid ) {
        if ( onDatabaseInteraction != null ) {
            onDatabaseInteraction.onFinish();
        }
    }
}
