package com.example.agenda.core.storage.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "Module",
        foreignKeys = @ForeignKey(entity = ScholarYear.class,
                                  parentColumns = "id",
                                  childColumns = "scholar_id",
                                  onDelete = ForeignKey.CASCADE))
public class Module implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long id;

    @ColumnInfo(name = "scholar_id")
    private long scholar_id;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "summary")
    private String summary;

    @Ignore
    public Module( String summary  ) {
        this.summary = summary;
        this.title = summary;
    }

    public Module(long id, String title, String summary) {
        this.id = id;
        this.title = title;
        this.summary = summary;
    }



    public long getId() { return id; }

    public String getTitle() { return title; }

    public String getSummary() {
        return summary;
    }

    public long getScholar_id() {
        return scholar_id;
    }

    public void setScholar_id(long scholar_id) {
        this.scholar_id = scholar_id;
    }

    public void setId(long id) { this.id = id; }

    public void setTitle(String title) { this.title = title; }

    public void setSummary( String summary ) {
        this.summary = summary;
    }

    @Override
    public String toString() {
        return "Module{" +
                "id=" + id +
                ", scholar_id=" + scholar_id +
                ", title='" + title + '\'' +
                ", summary='" + summary + '\'' +
                '}';
    }
}
