package com.example.agenda.core.storage.Dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.agenda.core.storage.Entity.Module;
import com.example.agenda.core.storage.Entity.ScholarYear;
import java.util.List;

/**
 * ScholarYearDao is an interface which implement basics queries for work with
 * ScholarYear's table
 **/
@Dao
public interface ScholarYearDao {

    /**
     * Give an ScholarYear based on his id
     * @param yearId the year's id searched
     * @return the year asked
     */
    @Query("SELECT * FROM ScholarYear WHERE id = :yearId")
    ScholarYear getScholarYear(long yearId);

    /**
     * Give an ScholarYear based on his title
     * @param yearTitle the year's title searched
     * @return the year asked
     */
    @Query("SELECT * FROM ScholarYear WHERE title = :yearTitle")
    ScholarYear getScholarYear(String yearTitle);

    /**
     * Give all events linked with a module
     * @param yearId the module's id
     * @return List<Event> an events list
     */
    @Query("SELECT * FROM Module WHERE scholar_id = :yearId")
    List<Module> getYearAllModule(long yearId);

    // I can also use LiveData, it's even recommended but I don't understand why...
    /**
     * Give all years stocked in the table
     * @return An years list
     */
    @Query("SELECT * FROM ScholarYear")
    List<ScholarYear> getAllScholarYear();

    /**
     * Insert an scholar year
     * @param scholarYear the year which will be inserted
     * @return year's id
     */
    @Insert
    long insert(ScholarYear scholarYear);

    /**
     * Update the year chosen
     * @param scholarYear the year which will be updated
     * @return I don't know ...
     */
    @Update
    int update(ScholarYear scholarYear);

    /**
     * Delete an year chosen
     * @param yearId the year's id which will be deleted
     * @return 1 if it's succeed 0 else I think
     */
    @Query("DELETE FROM ScholarYear WHERE id = :yearId")
    int delete(long yearId);

}
