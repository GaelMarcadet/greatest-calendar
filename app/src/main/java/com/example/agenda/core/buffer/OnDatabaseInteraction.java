package com.example.agenda.core.buffer;

/**
 * Notify listener when a database interaction starts and finish
 */
public interface OnDatabaseInteraction {
    void onStart();
    void onFinish();
}