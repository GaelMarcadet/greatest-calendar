package com.example.agenda.core.storage.Dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import com.example.agenda.core.storage.Entity.Event;

import java.util.List;

/**
 * EventDao is an interface which implement basics queries for work with
 * Event's table
 **/
@Dao
public interface EventDao {

    /**
     * Give an Event based on his id
     * @param eventId the event's id searched
     * @return the event asked
     */
    @Query("SELECT * FROM Event WHERE id = :eventId")
    Event getEvent(long eventId);

    // I can also use LiveData, it's even recommended but I don't understand why...
    /**
     * Give all events stocked in the table
     * @return An events list
     */
    @Query("SELECT * FROM Event")
    List<Event> getAllEvent();

    /**
     * Insert an event
     * @param event the event which will be inserted
     * @return event's id
     */
    @Insert
    long insert(Event event);

    /**
     * Update the event chosen
     * @param event the event which will be updated
     * @return I don't know ...
     */
    @Update
    int update(Event event);

    /**
     * Delete an event chosen
     * @param eventId the event's id which will be deleted
     * @return 1 if it's succeed 0 else I think
     */
    @Query("DELETE FROM Event WHERE id = :eventId")
    int delete(long eventId);
}
