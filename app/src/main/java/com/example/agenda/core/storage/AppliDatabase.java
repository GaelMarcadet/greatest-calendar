package com.example.agenda.core.storage;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.agenda.core.storage.Entity.Event;
import com.example.agenda.core.storage.Entity.Module;
import com.example.agenda.core.storage.Entity.ScholarYear;
import com.example.agenda.core.storage.Dao.EventDao;
import com.example.agenda.core.storage.Dao.ModuleDao;
import com.example.agenda.core.storage.Dao.ScholarYearDao;

/**
 *  Database containing Module, ProtoScholarYear and Event tables. It's an singleton pattern so it must
 *  be used with the method getInstance().
 */
@Database(entities = {Module.class, ScholarYear.class, Event.class},version = 3,exportSchema = false)
public abstract class AppliDatabase extends RoomDatabase {

    private static final String DB_NAME = "appli_db";

    // --- SINGLETON ---
    private static AppliDatabase INSTANCE;

    // --- DAO ---
    public abstract ModuleDao ModuleDao();
    public abstract EventDao EventDao();
    public abstract ScholarYearDao ScholarYearDao();

    // --- INSTANCE ---
    public static synchronized  AppliDatabase getInstance(Context context){
        if(INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),AppliDatabase.class,DB_NAME)
                    .fallbackToDestructiveMigration().build();
        }
        return INSTANCE;
    }

}
