package com.example.agenda.core.network;


import android.util.Log;

import com.example.agenda.core.storage.Entity.Event;
import com.example.agenda.core.storage.Entity.Module;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Parser dedicated for ADE Calendar.
 */
public class ADECalendarParser {

    private static final String CALENDAR_PARSER_TAG = "CALENDAR_PARSER";

    private static final String KEY_CALENDAR_NAME = "CN";
    private static final String SEPARATOR = ":";
    private static final String KEY_BEGIN = "BEGIN";
    private static final String KEY_CREATED = "CREATED";
    private static final String KEY_DESCRIPTION = "DESCRIPTION";
    private static final String KEY_LAST_MODIFIED = "LAST-MODIFIED";
    private static final String KEY_DTEND = "DTEND";
    private static final String KEY_DTSTART = "DTSTART";
    private static final String KEY_END = "END";
    private static final String KEY_ID = "ID";
    private static final String KEY_LOCATION = "LOCATION";
    private static final String KEY_SUMMARY = "SUMMARY";
    private static final String KEY_UID = "UID";
    private static final String VALUE_VEVENT = "VEVENT";
    private static final String EVENT_DATE_FORMAT = "yyyyMMdd'T'HHmmss'Z'";

    private boolean initialized;
    private List<Event> eventsContainer;
    private String calendarName;
    private SimpleDateFormat simpleDateFormat;
    private Iterable< String > lines;


    /**
     * Parse string lines into a calendar parser.
     *
     * @param lines Lines which must be parsed.
     *
     * @return The parser which has parse lines.
     *
     * @throws ParseException Invalid input.
     */
    public static ADECalendarParser parseCalendar( Iterable< String > lines ) throws ParseException {
        ADECalendarParser ADECalendarParser = new ADECalendarParser( lines );
        ADECalendarParser.parse();
        return ADECalendarParser;
    }

    public ADECalendarParser( Iterable< String > lines ) {
        this.simpleDateFormat = new SimpleDateFormat( EVENT_DATE_FORMAT );
        this.eventsContainer = new ArrayList<>();
        this.calendarName = "";
        this.lines = lines;
    }

    /**
     * Parse values and store the results.
     *
     * @throws ParseException
     */
    private void parse() throws ParseException {
        initialized = true;
        Event event = null;

        for ( String line : lines ) {



            line = clean( line );
            String[] lineInformation = line.split( SEPARATOR );
            String key = null;
            String value = null;

            try {
                key = lineInformation[ 0 ];
                value = lineInformation[ 1 ];
            } catch ( Exception e ) {
                Log.e( CALENDAR_PARSER_TAG, "parse error: " + e.getMessage() + " on key" + key  );
                continue;
            }

            Log.d( CALENDAR_PARSER_TAG, "Parsing line {key: \"" + key + "\", value: \"" + value + "\"" );


            // parsing key calendar
            if ( key.equals( KEY_CALENDAR_NAME ) ) {
                this.calendarName = value;
                continue;
            }

            // starting parsing events
            if ( key.equals( KEY_BEGIN ) ) {
                if ( value.equals( VALUE_VEVENT ) ) {
                    event = new Event();
                    continue;
                }
            }

            // ending parsing events
            if ( key.equals( KEY_END ) ) {
                if ( value.equals( VALUE_VEVENT ) ) {
                    Log.d( CALENDAR_PARSER_TAG, "parse: finalize an event: " + event.getId() );
                    this.eventsContainer.add( event );
                    event = null;
                    continue;
                }
            }

            // parsing events information
            if ( event != null ) {
                if ( key.equals( KEY_CREATED ) ) {
                    event.setCreatedAt( parseDate( value ) );
                } else if ( key.equals( KEY_DTSTART ) ) {
                    event.setStartAt( parseDate( value ) );
                } else if ( key.equals( KEY_DTEND ) ) {
                    event.setEndAt( parseDate( value ) );
                } else if ( key.equals( KEY_LAST_MODIFIED ) ) {
                    event.setModifiedAt( parseDate( value ) );
                } else if ( key.equals( KEY_DESCRIPTION ) ) {
                    event.setDescription( value );
                } else if ( key.equals( KEY_LOCATION ) ) {
                    event.setLocation( value );
                } else if ( key.equals( KEY_SUMMARY ) ) {
                    event.setSummary( value );
                } else if ( key.equals( KEY_UID ) ) {
                    event.setUid( value );
                    event.setGlobalId( parseIDFromUID( value ) );
                }
            }
        }
    }

    public String getCalendarName()  {
        if ( !this.isInitialized() ) {
            throw new IllegalStateException();
        }
        return this.calendarName;
    }


    public List<Event> getEvents()  {
        if ( !this.isInitialized() ) {
            throw new IllegalStateException();
        }
        return this.eventsContainer;
    }

    public List<Module> getModules() {
        // a module is created when an event is linked with, but not already registered
        HashMap<String, Module> modulesBySummary = new HashMap<>();
        for ( Event event : this.eventsContainer ) {
            String eventSummary = event.getSummary();
            if ( !modulesBySummary.containsKey( eventSummary ) ) {
                modulesBySummary.put( eventSummary, new Module( eventSummary ) );
            }
        }

        return new ArrayList<>( modulesBySummary.values() );
    }

    private long parseIDFromUID( String uid ) {
        // awaiting uid like
        // EDTWeb-2019-2020-EVENT-41740@univ-orleans.fr
        String domain = uid.split( "@" )[ 0 ];
        String id = domain.split( "-" )[ 4 ];
        return Long.parseLong( id );
    }


    private Date parseDate( String date ) throws ParseException {
        return this.simpleDateFormat.parse( date );
    }

    private boolean isInitialized() {
        return this.initialized;
    }

    private static final char RETURN_CHAR= '\n';
    private static final char DOUBLE_QUOTE_CHAR = '"';

    private synchronized String clean( String s ) {
        s = s.trim();
        int length = s.length();

        StringBuilder builder = new StringBuilder();
        for ( int index = 0; index < length; index++ ) {
            char c = s.charAt( index );

            // ignoring return characters when at the end of the line
            if ( c == RETURN_CHAR && index + 1 == length ) {
                continue;
            }

            // ignore double quote char
            if ( c == DOUBLE_QUOTE_CHAR ) {
                continue;
            }

            builder.append( c );
        }

        return builder.toString();

    }


}
