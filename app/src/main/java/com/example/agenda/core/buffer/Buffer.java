package com.example.agenda.core.buffer;

import android.os.AsyncTask;
import android.util.Log;

import com.example.agenda.core.adapter.SortedListAdapter;
import com.example.agenda.core.storage.Entity.Event;
import com.example.agenda.core.storage.Entity.Module;
import com.example.agenda.core.storage.Entity.ScholarYear;
import com.example.agenda.core.storage.Facade.StorageInterface;

import java.util.ArrayList;
import java.util.List;

public class Buffer {
    private List<ScholarYear> scholarYears;
    private List<Module> modules;
    private List<Event> events;

    private static Buffer instance;

    public Buffer() {
        this.scholarYears = new ArrayList<>();
        this.modules = new ArrayList<>();
        this.events = new ArrayList<>();
    }

    public void logBufferContent() {
        final String bufferLog = "BUFFER-DEBUG";

        Log.d( bufferLog, "====================================" );
        // log scholar years data
        Log.d( bufferLog, this.scholarYears.size() + " scholar years saved" );
        Log.d( bufferLog, this.scholarYears.toString() );


        // log modules data
        Log.d( bufferLog, this.modules.size() + " modules saved" );
        Log.d( bufferLog, this.modules.toString() );

        // log events data
        Log.d( bufferLog, this.events.size() + " events saved" );
        Log.d( bufferLog, this.events.toString() );

    }

    public List<ScholarYear> getScholarYears() {
        return new ArrayList<>( scholarYears );
    }

    public List<Module> getModules() {
        return new ArrayList<>( modules );
    }

    public Module getModule(long module_id){
        for ( Module module : modules) {
            if(module.getId() == module_id){
                return module;
            }
        }
        return null;
    }

    public List<Event> getEvents() {
        return new ArrayList<>( events );
    }

    public void addScholarYear( ScholarYear scholarYear ) {
        this.scholarYears.add( scholarYear );
    }

    public void updateModule( String summary ) {
        this.updateModule( new Module( summary ) );
    }


    public void updateScholarYear( ScholarYear updatedScholarYear ) {
        for ( ScholarYear scholarYear : this.scholarYears ) {
            if ( scholarYear.equals( updatedScholarYear ) ) {
                updatedScholarYear.setId( scholarYear.getId() );
                this.scholarYears.remove( scholarYear );
                break;
            }
        }
        this.scholarYears.add( updatedScholarYear );
    }

    public void updateModule( Module module ) {
        for ( Module m : modules ) {
            if ( m.getSummary().equals( module.getSummary() ) ) {
                module.setId( m.getId() );
                module.setScholar_id( m.getScholar_id() );
                modules.remove( m );
                break;
            }
        }
        modules.add( module );
    }


    public void updateEvent( Event newEvent ) {
        for ( Event event : events ) {
            if ( event.getGlobalId() == newEvent.getGlobalId() ) {
                newEvent.setId( event.getId() );
                newEvent.setModule_id( event.getModule_id() );
                events.remove( event );
                events.add( newEvent );
                return;
            }
        }
        events.add( newEvent );
    }

    public void saveAndReload( StorageInterface storage, OnDatabaseInteraction onDatabaseInteraction ) {
        BufferSaver saver = new BufferSaver( storage );
        BufferLoader loader = new BufferLoader( storage );
        saver.setOnDatabaseInteraction( new OnDatabaseInteraction() {
            @Override
            public void onStart() {
                onDatabaseInteraction.onStart();
            }

            @Override
            public void onFinish() {
                loader.execute();
            }
        } );

        loader.setOnDatabaseInteraction( new OnDatabaseInteraction() {
            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {
                onDatabaseInteraction.onFinish();
            }
        } );

        saver.execute();
    }

    public void setScholarYears( List<ScholarYear> scholarYears ) {
        this.scholarYears = scholarYears;
    }

    public void setModules( List<Module> modules ) {
        this.modules = modules;
    }

    public void setEvents( List<Event> events ) {
        this.events = events;
    }

    public static Buffer getInstance() {
        if ( instance == null ) {
            instance = new Buffer();
        }
        return instance;
    }


    private boolean moduleExists( String summary ) {
        for ( Module module : modules ) {
            if ( module.getSummary().equals( summary ) ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Give a list of module with teh same year_id given in parameter
     * @param year_id a parameter filter
     * @return A list of module filtered with the function's parameter and return
     * the whole list if the parameter is equal to -1
     */
    public List<Module> getModulesbyId(long year_id) {
        if(year_id == -1) return modules;

        List<Module> module_list = new ArrayList<>();

        for(Module module : modules){
            if(module.getScholar_id() == year_id){
                module_list.add(module);
            }
        }

        return module_list;
    }

    public List<Event> getEventsById(long module_id) {
        if(module_id == -1) return events;

        List<Event> event_list = new ArrayList<>();

        for(Event event : events){
            if(event.getModule_id() == module_id){
                event_list.add(event);
            }
        }

        return event_list;
    }

    public ScholarYear getScholarYearById(long year_id) {
        if(year_id == -1) return new ScholarYear();

        for (ScholarYear scholarYear : scholarYears){
            if(scholarYear.getId() == year_id){
                return scholarYear;
            }
        }
        return null;
    }

    public Module getModulebyId(long module_id) {
        if(module_id == -1) return null;

        for (Module module : modules){
            if(module.getId() == module_id){
                return module;
            }
        }
        return null;
    }

}
