package com.example.agenda.core.storage.Facade;

import com.example.agenda.core.storage.Entity.Event;
import com.example.agenda.core.storage.Entity.Module;
import com.example.agenda.core.storage.Entity.ScholarYear;

import java.util.List;

/**
 * Storage interface with CRUD standards operations.
 */
public interface StorageInterface {

    /**
     * Returns all events stored in database.
     *
     * @return All events stored in database.
     */
    List<Event> getAllEvents();

    /**
     * Returns event bound with provided id.
     *
     * @param eventId Event id.
     *
     * @return Event bound with provided id.
     */
    Event getEvent( long eventId );

    /**
     * Returns Module which has provided name.
     *
     * @param moduleName Module's name.
     *
     * @return Module which has provided name.
     */
    Module getModule( String moduleName );

    /**
     * Returns all modules stored in database.
     *
     * @return All modules stored in database.
     */
    List<Module> getAllModules();

    /**
     * Returns scholar year which has provided name.
     *
     * @param name Scholar year's name.
     *
     * @return The scholar year which has provided name.
     */
    ScholarYear getScholarYear( String name );

    /**
     * Returns all scholar years stored in database.
     *
     * @return All scholar years stored in database.
     */
    List<ScholarYear> getAllScholarYears();

    /**
     * Inserts an event in database.
     *
     * @param event Inserted event.
     */
    void insertEvent( Event event );

    /**
     * Inserts a module in database.
     *
     * @param module Inserted module.
     */
    void insertModule( Module module );

    /**
     * Inserts a scholar year in database.
     *
     * @param scholarYear Inserted scholar year.
     */
    void insertScholarYear( ScholarYear scholarYear );

    /**
     * Update an existing event in database.
     *
     * To work properly, he uses event's id to find and update entry in database.
     *
     * @param event Event to update.
     */
    void updateEvent( Event event );

    /**
     * Update an existing module in database.
     *
     * To work properly, he uses module's id to find and update entry in database.
     *
     * @param module Module to update.
     */
    void updateModule( Module module );

    /**
     * Update an existing scholar year in database.
     *
     * To work properly, he uses scholar year's id to find and update entry in database.
     *
     * @param scholarYear Scholar year to update.
     */
    void updateScholarYear( ScholarYear scholarYear );

    /**
     * Deletes an event from database.
     *
     * @param event Deleted event.
     */
    void deleteEvent( Event event );

    /**
     * Deletes a module from database.
     *
     * @param module Deleted module.
     */
    void deleteModule( Module module );

    /**
     * Deletes a scholar year from database.
     *
     * @param scholarYear Deleted scholar year.
     */
    void deleteScholarYear( ScholarYear scholarYear );
}
