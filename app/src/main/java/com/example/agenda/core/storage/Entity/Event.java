package com.example.agenda.core.storage.Entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

@Entity(tableName = "Event",
        foreignKeys =
            @ForeignKey(entity =  Module.class,
                        parentColumns = "id",
                        childColumns = "module_id",
                        onDelete = ForeignKey.CASCADE))
/*
 * An event is a teaching defined with a location, a date of beginning and ending.
 * The description has a description and a summary.
 */
public class Event implements Comparable< Event >, Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long id;

    @ColumnInfo(name = "module_id")
    private long module_id;

    @ColumnInfo(name = "global_id")
    private long globalId;

    @ColumnInfo(name = "uid")
    private String uid;

    @ColumnInfo(name = "startAt")
    @TypeConverters({Converters.class})
    private Date startAt;

    @ColumnInfo(name = "endAt")
    @TypeConverters({Converters.class})
    private Date endAt;

    @ColumnInfo(name = "createdAt")
    @TypeConverters({Converters.class})
    private Date createdAt;

    @ColumnInfo(name = "modifiedAt")
    @TypeConverters({Converters.class})
    private Date modifiedAt;

    @ColumnInfo(name = "location")
    private String location;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "summary")
    private String summary;

    @Ignore
    public Event(){ }

    public Event(long id, long module_id, String uid, Date startAt, Date endAt, Date createdAt, Date modifiedAt, String location, String description, String summary) {
        this.id = id;
        this.module_id = module_id;
        this.uid = uid;
        this.startAt = startAt;
        this.endAt = endAt;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
        this.location = location;
        this.description = description;
        this.summary = summary;
    }

    @Override
    public int compareTo( Event o ) {
        if ( this.getGlobalId() == o.getGlobalId() ) {
            return 0;
        } else {
            return this.getStartAt().compareTo( o.getStartAt() );
        }
    }

    public long getGlobalId() {
        return globalId;
    }

    public void setGlobalId( long globalId ) {
        this.globalId = globalId;
    }

    public long getId() {
        return id;
    }

    public String getUid() {
        return uid;
    }

    public Date getStartAt() {
        return startAt;
    }

    public Date getEndAt() {
        return endAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public String getSummary() {
        return summary;
    }

    public long getModule_id() {
        return module_id;
    }

    public void setModule_id(long module_id) {
        this.module_id = module_id;
    }

    public void setId(long id ) {
        this.id = id;
    }



    public void setUid( String uid ) {
        this.uid = uid;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public void setEndAt(Date endAt) {
        this.endAt = endAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public void setLocation( String location ) {
        this.location = location;
    }

    public void setDescription( String description ) {
        this.description = description;
    }

    public void setSummary( String summary ) {
        this.summary = summary;
    }

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) return true;
        if ( !( o instanceof Event ) ) return false;
        Event event = ( Event ) o;
        return getGlobalId() == event.getGlobalId();
    }

    @Override
    public int hashCode() {
        return Objects.hash( getGlobalId() );
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", module_id=" + module_id +
                ", globalId=" + globalId +
                ", uid='" + uid + '\'' +
                ", startAt=" + startAt +
                ", endAt=" + endAt +
                ", createdAt=" + createdAt +
                ", modifiedAt=" + modifiedAt +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                ", summary='" + summary + '\'' +
                '}';
    }
}


