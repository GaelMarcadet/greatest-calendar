package com.example.agenda.activities.scholar_years.display;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.agenda.R;
import com.example.agenda.core.adapter.GenericViewHolder;
import com.example.agenda.core.storage.Entity.ScholarYear;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Generic view holder specialized for scholar year.
 * A view holder must wrap view content initialization, as simple as getting view item and
 * initialize this content.
 */
public class ScholarYearViewHolder extends GenericViewHolder<ScholarYear> {

    /**
     * Scholar year name text view.
     */
    private TextView scholarYearName;

    /**
     * Scholar year last synchronisation text view.
     */
    private TextView lastSync;

    /**
     * Default constructor which initialize view.
     *
     * @param itemView Scholar year view.
     */
    public ScholarYearViewHolder( @NonNull View itemView ) {
        super( itemView );
        this.scholarYearName = itemView.findViewById( R.id.scholar_year_name );
        this.lastSync = itemView.findViewById( R.id.scholar_year_last_sync );
    }


    @Override
    public void updateViewHolder( ScholarYear scholarYear ) {
        this.scholarYearName.setText( scholarYear.getTitle() );

        // format last sync date
        DateFormat format = new SimpleDateFormat( ScholarYear.DATE_FORMAT, Locale.getDefault() );
        String lastSyncText = format.format( scholarYear.getLastSynchronize().getTime() );
        String lastSyncFormat = getContext().getResources().getString( R.string.scholar_year_last_sync );

        this.lastSync.setText( String.format( lastSyncFormat, lastSyncText ) );
    }
}
