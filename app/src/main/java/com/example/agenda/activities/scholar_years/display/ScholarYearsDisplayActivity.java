package com.example.agenda.activities.scholar_years.display;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.agenda.R;
import com.example.agenda.activities.modules.display.ModuleDisplayActivity;
import com.example.agenda.activities.scholar_years.creation.CalendarCreationActivity;
import com.example.agenda.activities.scholar_years.creation.DownloadLifeListener;
import com.example.agenda.core.buffer.Buffer;
import com.example.agenda.core.buffer.BufferSaver;
import com.example.agenda.core.buffer.OnDatabaseInteraction;
import com.example.agenda.core.network.ADECalendarDownloader;
import com.example.agenda.core.network.ADECalendarParser;
import com.example.agenda.core.storage.Entity.Event;
import com.example.agenda.core.storage.Entity.ScholarYear;
import com.example.agenda.core.storage.Facade.Storage;

import java.util.ArrayList;
import java.util.List;


/**
 * Display a list of scholar years. Can also creates a new scholar year.
 */
public class ScholarYearsDisplayActivity extends AppCompatActivity
        implements OnScholarYearInteraction {

    /**
     * Internet permission request code.
     */
    private static final int INTERNET_PERMISSION_REQUEST = 159;

    /**
     * Calendar creation request code.
     */
    private static final int REQUEST_CALENDAR_CREATION = 294;

    /**
     * List of scholar years.
     */
    private List< ScholarYear > scholarYearList;

    /**
     * List view used to display some scholar years
     */
    private RecyclerView recyclerView;

    /**
     * Adapter fot the list view.
     */
    private ScholarYearsAdapter scholarYearsAdapter;

    /**
     * Progress bar to notify scholar_year_background processing.
     */
    private AlertDialog processInProgessDialog;


    public static AlertDialog buildCalendarDownloadDialog( Context context ) {
        AlertDialog.Builder builder = new AlertDialog.Builder( context );
        builder.setTitle( R.string.calendar_download_popup_title );
        builder.setMessage( R.string.calendar_download_downloading );
        return builder.create();
    }

    /**
     * Default constructor.
     */
    public ScholarYearsDisplayActivity() {
        this.scholarYearList = new ArrayList<>();
    }

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {

        Buffer.getInstance().logBufferContent();
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        startToolbar();

        this.scholarYearList = Buffer.getInstance().getScholarYears();

        // Creating views
        this.recyclerView = findViewById( R.id.years_recycler_view );
        this.recyclerView.setLayoutManager( new LinearLayoutManager( this ) );

        this.scholarYearsAdapter = new ScholarYearsAdapter( this.scholarYearList );
        this.scholarYearsAdapter.addObserver( this );
        this.recyclerView.setAdapter( scholarYearsAdapter );
        this.processInProgessDialog = buildCalendarDownloadDialog( this );
    }

    private void startToolbar() {
        Toolbar toolbar = findViewById( R.id.tool_bar );
        this.setSupportActionBar( toolbar );

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate( R.menu.menu_main, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item) {
        int id = item.getItemId();

        // handle calendar creation request
        if ( id == R.id.menu_calendar_create) {
            startCalendarCreationActivity();
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Start the activity to create a new calendar
     */
    private void startCalendarCreationActivity() {
        Intent intent = new Intent( this, CalendarCreationActivity.class );
        startActivityForResult( intent, REQUEST_CALENDAR_CREATION );
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, @Nullable Intent data ) {
        super.onActivityResult( requestCode, resultCode, data );
        switch ( requestCode ) {
            case REQUEST_CALENDAR_CREATION:
                onScholarYearCreation( resultCode, data );
                break;
        }
    }

    @Override
    public void onItemInteraction( ScholarYear scholarYear ) {
        Log.i( "SCHOLAR_YEAR_INTERACT", "onItemInteraction: Starting "
                + scholarYear.getTitle() );
        if ( !scholarYear.isSynchronized() ) {
            Log.i( "SCHOLAR_YEAR_INTERACT", "Scholar year not synchronized: " + scholarYear );
            synchronizeScholarYear( scholarYear );
        } else {
            Log.i( "SCHOLAR_YEAR_INTERACT", "Display in progress" );
            displayScholarYear( scholarYear );
        }
    }

    /**
     * Handle scholar year creation result.
     *
     * @param resultCode Results of the scholar year creation request.
     * @param data Data given by activity.
     */
    private void onScholarYearCreation( int resultCode, @Nullable Intent data ) {
        if ( resultCode == CalendarCreationActivity.CALENDAR_CREATED_RESPONSE ) {
            ScholarYear scholarYear = ( ScholarYear ) data.getSerializableExtra(
                    CalendarCreationActivity.SCHOLAR_YEAR_EXTRA );
            this.scholarYearsAdapter.add( scholarYear );
        }
    }

    /**
     * Synchronize a scholar year with the online version.
     *
     * @param scholarYear Scholar year to synchronize.
     */
    private void synchronizeScholarYear( ScholarYear scholarYear ) {
        // synchronisation require internet to work
        if ( !hasInternetPermission() ) {
            this.requireInternetPermission();
        }

        ScholarYearSynchronizer synchronizer = new ScholarYearSynchronizer( scholarYear );
        synchronizer.synchronize();
    }

    /**
     * Returns {@code true} if and only if the user have granted application
     * to dealing with internet.
     *
     * @return Returns {@code true} if and only if the user have granted application
     *      to dealing with internet, {@code false} otherwise.
     */
    private boolean hasInternetPermission() {
        return ContextCompat.checkSelfPermission( this, Manifest.permission.INTERNET )
                == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Request the internet permission to the user.
     */
    private void requireInternetPermission() {
        ActivityCompat.requestPermissions( this,
                new String[] { Manifest.permission.INTERNET },
                INTERNET_PERMISSION_REQUEST );
    }

    /**
     * Start the activity in charge to display a scholar year.
     *
     * @param scholarYear The scholar year to display.
     */
    private void displayScholarYear( ScholarYear scholarYear ) {
        Log.d( "SCHOLAR_YEAR_DISP", "Displaying scholar year " + scholarYear.getTitle() + " id " + scholarYear.getId());
        Intent intent = new Intent(this, ModuleDisplayActivity.class);
        intent.putExtra("year_id",scholarYear.getId());
        startActivity(intent);
    }


    /**
     * Synchronizer in charge to synchronize a scholar year with the online version.
     */
    private class ScholarYearSynchronizer implements DownloadLifeListener<ADECalendarParser>, OnDatabaseInteraction {

        /**
         * The scholar year to synchronize.
         */
        private ScholarYear scholarYear;

        /**
         * Default constructor
         *
         * @param scholarYear The scholar year to synchronize.
         */
        public ScholarYearSynchronizer( ScholarYear scholarYear ) {
            this.scholarYear = scholarYear;
        }

        /**
         * Start the synchronization process.
         */
        public void synchronize() {
            ADECalendarDownloader ADECalendarDownloader = new ADECalendarDownloader( getCalendarURL() ,this );
            ADECalendarDownloader.execute();
        }

        @Override
        public void onDownloadStart() {
            processInProgessDialog.setMessage( getText( R.string.calendar_download_downloading ) );
            processInProgessDialog.show();
        }

        @Override
        public void onDownloadSuccess( ADECalendarParser value ) {
            Buffer buffer = Buffer.getInstance();
            for ( Event event : value.getEvents() ) {
                buffer.updateEvent( event );
            }
            this.scholarYear.notifyAsSynchronized();
        }

        @Override
        public void onDownloadFailed() {

        }

        @Override
        public void onPostDownload() {
            processInProgessDialog.setMessage( getText( R.string.calendar_download_parsing ) );
        }

        @Override
        public void onDownloadFinish() {
            // update buffer and save buffer into database
            Buffer buffer = Buffer.getInstance();
            buffer.updateScholarYear( scholarYear );

            // save
            BufferSaver saver = new BufferSaver( new Storage( getApplicationContext() ) );
            saver.setOnDatabaseInteraction( this );
            saver.execute();

        }

        public ScholarYear getScholarYear() {
            return this.scholarYear;
        }

        /**
         * Provides the targeted url which provides calendar.
         *
         * @return The url which provides the calendar.s
         */
        private String getCalendarURL() {
            return this.scholarYear.getCalendarURL();
        }

        @Override
        public void onStart() {

        }

        @Override
        public void onFinish() {
            processInProgessDialog.dismiss();
            displayScholarYear( this.scholarYear );

            // clearing adapter to add buffer's scholar years instead.
            Buffer buffer = Buffer.getInstance();
            scholarYearsAdapter.clear();
            scholarYearsAdapter.addAll( buffer.getScholarYears() );
        }
    }


}
