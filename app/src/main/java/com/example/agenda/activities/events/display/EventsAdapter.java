package com.example.agenda.activities.events.display;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.agenda.R;
import com.example.agenda.core.adapter.GenericViewHolder;
import com.example.agenda.core.adapter.SortedListAdapter;
import com.example.agenda.core.storage.Entity.Event;

import java.util.List;

public class EventsAdapter extends SortedListAdapter<Event> {


    EventsAdapter(List<Event> events) {
        super(events);
    }


    @Override
    public GenericViewHolder createViewHolder(View view) {
        return new EventViewHolder(view);
    }


    @Override
    public View inflateView(ViewGroup parent, LayoutInflater layoutInflater) {
        return layoutInflater.inflate(R.layout.fragment_event,parent,false);
    }



}
