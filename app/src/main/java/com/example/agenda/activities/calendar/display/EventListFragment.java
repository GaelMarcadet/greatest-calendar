package com.example.agenda.activities.calendar.display;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.agenda.R;
import com.example.agenda.core.storage.Entity.Event;

import java.util.ArrayList;
import java.util.List;

public class EventListFragment extends Fragment {


    private List<Event> eventList;

    private  EventListAdapter eventListAdapter;



    public EventListFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_calendar_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            Log.i("CONTEXT EVENT LIST",context.toString());
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            if(eventListAdapter == null){
                eventListAdapter = new EventListAdapter(eventList);
                recyclerView.setAdapter(eventListAdapter);
            }

        }
        return view;
    }


    public void update(List<Event> events) {
        eventList.addAll(events);
    }
}
