package com.example.agenda.activities.scholar_years.creation;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.agenda.R;
import com.example.agenda.core.buffer.Buffer;
import com.example.agenda.core.storage.Entity.Event;
import com.example.agenda.core.storage.Entity.Module;
import com.example.agenda.core.storage.Entity.ScholarYear;
import com.example.agenda.core.network.ADECalendarDownloader;
import com.example.agenda.core.network.ADECalendarParser;
import com.example.agenda.core.storage.Facade.Storage;
import com.example.agenda.core.storage.Facade.StorageInterface;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * This activity ensures calendar creation.
 */
public class CalendarCreationActivity extends AppCompatActivity
        implements DownloadLifeListener<ADECalendarParser> {

    /**
     * Calendar creation response.
     * Assigned to valid calendar creation.
     */
    public static final int CALENDAR_CREATED_RESPONSE = 351;

    /**
     * Calendar creation response;
     * Assigned to invalid calendar creation.
     */
    public static final int CALENDAR_DISMISSED_RESPONSE = 352;

    private static final String TEXT_URL_PREFERENCES_KEY = "text-url";
    private static final String SHARED_PREFERENCES_NAME = "calendar-creation-preferences";

    /**
     * Extra tag for scholar year.
     */
    public static final String SCHOLAR_YEAR_EXTRA = "scholar_year_extra";

    /**
     * Internet permission request.
     */
    private static final int INTERNET_PERMISSION_REQUEST = 159;

    /**
     * Text field fot calendar url.
     */
    private TextView calendarTextURL;

    /**
     * Button assigned to download starting.
     */
    private Button startDownloadButton;

    /**
     * Process in progress state.
     */
    private boolean processInProgress;

    /**
     * Generic dialog to notify (and block) user during critical internal process.
     */
    private AlertDialog genericDialog;


    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_year_creation );
        this.setTitle( getText( R.string.calendar_creation_interface_title ) );
        this.initiateToolbar();

        // mounting fields
        this.genericDialog = buildGenericDialog();
        this.calendarTextURL = findViewById( R.id.calendar_creation_url );
        this.startDownloadButton = findViewById( R.id.calendar_creation_download );
        this.startDownloadButton.setOnClickListener( v -> onDownloadRequired() );

        // initialize text url with user preferences
        SharedPreferences sharedPreferences = getSharedPreferences( SHARED_PREFERENCES_NAME, MODE_PRIVATE );
        this.calendarTextURL.setText( sharedPreferences.getString( TEXT_URL_PREFERENCES_KEY, "" ) );
    }

    /**
     * Initial toolbar component.
     */
    private void initiateToolbar() {
        Toolbar toolbar = findViewById( R.id.tool_bar_calendar_creation );
        this.setSupportActionBar( toolbar );
        // enable full toolbar support
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );
        getSupportActionBar().setDisplayShowHomeEnabled( true );
    }

    /**
     * Returns a generic dialog.
     * The built dialog is not cancelable by user.
     *
     * @return A generic dialog.
     */
    private AlertDialog buildGenericDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setTitle( getText( R.string.calendar_download_popup_title ) );
        builder.setCancelable( false );
        return builder.create();
    }

    /**
     * Required a calendar download.
     */
    private void onDownloadRequired() {
        Log.d( "DOWNLOAD_REQUIRED", "onDownloadRequired: " + hasInternetPermission() );
        if ( hasInternetPermission() ) {
            startDownload();
        } else {
            requireInternetPermission();
        }
    }

    /**
     * Starts calendar download.
     */
    private void startDownload() {
        Log.w( "DOWNLOAD_STARTED", "startDownload: " );
        String calendarURL = this.calendarTextURL.getText().toString();

        ADECalendarDownloader downloader = new ADECalendarDownloader( calendarURL, this );
        downloader.execute();
    }

    /**
     * Returns {@code true} if and only if internet permission has been granted.
     *
     * @return {@code true} if and only if internet permission has been granted, else {@code false} otherwise.
     */
    private boolean hasInternetPermission() {
        return ContextCompat.checkSelfPermission( this, Manifest.permission.INTERNET )
                == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Requires internet permission.
     */
    private void requireInternetPermission() {
        ActivityCompat.requestPermissions( this,
                new String[] { Manifest.permission.INTERNET },
                INTERNET_PERMISSION_REQUEST );
    }

    @Override
    public void onRequestPermissionsResult( int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults ) {
        super.onRequestPermissionsResult( requestCode, permissions, grantResults );
    }

    @Override
    public void onDownloadStart() {
        this.processInProgress = true;

        // show download popup
        this.genericDialog.setTitle( getText( R.string.calendar_download_popup_title ) );
        this.genericDialog.setMessage( getText( R.string.calendar_download_downloading ) );
        this.genericDialog.show();
    }

    @Override
    public void onDownloadSuccess( ADECalendarParser parser ) {
        ScholarYearPostProcess checker = new ScholarYearPostProcess( parser );
        checker.execute();
    }

    @Override
    public void onDownloadFailed() {
        this.calendarTextURL.setError( getText( R.string.calendar_creation_invalid_text ) );
    }

    @Override
    public void onDownloadFinish() {
    }

    @Override
    public void onPostDownload() {
        this.genericDialog.setMessage( getText( R.string.calendar_download_parsing ) );
    }


    /**
     * Finish creation activity with a scholar year results.
     *
     * @param scholarYear Created scholar year.
     */
    private void finishActivityWithResult( ScholarYear scholarYear ) {
        Intent intent = new Intent();
        intent.putExtra( SCHOLAR_YEAR_EXTRA, scholarYear );
        setResult( CALENDAR_CREATED_RESPONSE, intent );
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // saving calendar text url in shared preferences
        SharedPreferences sharedPreferences = getSharedPreferences( SHARED_PREFERENCES_NAME, MODE_PRIVATE );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString( TEXT_URL_PREFERENCES_KEY, this.calendarTextURL.getText().toString() );
        editor.commit();
    }

    @Override
    public boolean onNavigateUp() {
        setResult( CALENDAR_DISMISSED_RESPONSE );
        return super.onNavigateUp();
    }

    /**
     * Returns a scholar year created from a calendar parser.
     *
     * @param parser Calendar parser used to build scholar year.
     * @return A scholar year.
     */
    private ScholarYear buildScholarYear( ADECalendarParser parser ) {
        ScholarYear scholarYear = new ScholarYear();

        scholarYear.setInitialYear( Calendar.getInstance().get( Calendar.YEAR ) );
        scholarYear.setTitle( parser.getCalendarName() );
        // scholarYear.addAllEvents( parser.getEvents() );
        scholarYear.setCalendarURL( this.calendarTextURL.getText().toString() );
        scholarYear.notifyAsSynchronized();

        return scholarYear;
    }

    @Override
    public boolean onSupportNavigateUp() {
        // only close activity when no one process is in progress
        if ( !processInProgress ) {
            onBackPressed();
        }

        return true;
    }

    /**
     * Provides scholar year validation.
     */
    private class ScholarYearPostProcess extends AsyncTask<Void, Void, Boolean> {

        private ADECalendarParser parser;
        private ScholarYear scholarYear;

        /**
         * Storage system.
         */
        private StorageInterface storage;




        ScholarYearPostProcess(ADECalendarParser parser) {

            this.scholarYear = buildScholarYear( parser );
            this.parser = parser;
            this.storage = new Storage( getApplicationContext() );
        }

        @Override
        protected void onPreExecute() {
            paintDialog();
        }

        @Override
        protected Boolean doInBackground( Void... voids ) {
            // checks if scholar year exists or not
            String title = parser.getCalendarName();
            for ( ScholarYear scholarYear : Buffer.getInstance().getScholarYears() ) {
                if ( scholarYear.getTitle().equals( title ) ) {
                    return true;
                }
            }

            // loading all elements in buffer

            // insert scholar year
            Buffer buffer = Buffer.getInstance();
            storage.insertScholarYear( scholarYear );
            buffer.addScholarYear( scholarYear );


            List<Module> moduleList = parser.getModules();
            for ( Module module : moduleList ) {
                module.setScholar_id( scholarYear.getId() );
                storage.insertModule( module );
                buffer.updateModule( module );
            }

            for ( Event event : parser.getEvents() ) {
                for ( Module module : moduleList ) {
                    if ( event.getSummary().equals( module.getSummary() ) ) {
                        event.setModule_id( module.getId() );
                    }
                }

                // TODO improve date fields's event integrity verification
                if ( event.getCreatedAt() == null ) {
                    Log.w( "DATA_INTEGRITY", "Event creation date is null" );
                    event.setCreatedAt( new Date() );
                }

                if ( event.getStartAt() == null ) {
                    Log.w( "DATA_INTEGRITY", "Event start date is null" );
                    event.setStartAt( new Date() );
                }

                if ( event.getEndAt() == null ) {
                    Log.w( "DATA_INTEGRITY", "Event end date is null" );
                    event.setEndAt( new Date() );
                }

                if ( event.getModifiedAt() == null ) {
                    Log.w( "DATA_INTEGRITY", "Event modification date is null" );
                    event.setModifiedAt( new Date() );
                }

                storage.insertEvent( event );
                buffer.updateEvent( event );
            }

            return false;
        }

        @Override
        protected void onPostExecute( Boolean exists ) {
            processInProgress = false;
            genericDialog.dismiss();
            if ( !exists ) {
                finishActivityWithResult( scholarYear );
            } else {
                Toast.makeText( getApplicationContext(), R.string.calendar_creation_error_exists, Toast.LENGTH_LONG ).show();
            }
        }



        /**
         * Repaint dialog to notify unicity checking.
         */
        private void paintDialog() {
            genericDialog.setTitle( getText( R.string.dialog_title_unicity_checking ) );
            genericDialog.setMessage( getText( R.string.dialog_message_unicity_checking ) );
        }
    }
}
