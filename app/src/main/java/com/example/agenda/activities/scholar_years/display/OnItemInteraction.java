package com.example.agenda.activities.scholar_years.display;

/**
 * Handle an interaction event with a generic item.
 * All kind of interaction events can be handled by the functional interface.
 * However, the interaction must returns a object to be used.
 *
 * @param <Item> Object given by the results of interaction.
 */
@FunctionalInterface
public interface OnItemInteraction< Item > {

    /**
     * Handle interaction with an item.
     *
     * @param item The interacted item.
     */
    void onItemInteraction( Item item );
}
