package com.example.agenda.activities.calendar.display;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import androidx.fragment.app.Fragment;
import com.example.agenda.R;

public class CalendarFragment extends Fragment implements View.OnClickListener {


    private CalendarView calendarView;

    public CalendarFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);

        Log.i("START CALENDAR","display calendar");

        this.calendarView = view.findViewById(R.id.event_calendar);

        calendarView.setOnDateChangeListener((calendarView, i, i1, i2) -> {

            String msg = "Selected date Day: " + i2 + " Month : " + (i1 + 1) + " Year " + i;
            Log.i("DAY SELECTED",msg);
        });
        return view;
    }

    @Override
    public void onClick(View v) {

    }
}
