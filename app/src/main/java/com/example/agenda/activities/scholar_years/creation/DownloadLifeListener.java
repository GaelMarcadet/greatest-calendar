package com.example.agenda.activities.scholar_years.creation;

/**
 * Download life listener is used to handle download cycle life events.
 * At the beginning, the download is started. When document is downloaded, a
 * post-processing is executing to interpret results. The download is notify as finished when
 * all data are downloaded and post-processed.
 *
 * @param <T> Value returned by post-process.
 */
public interface DownloadLifeListener< T > {
    /**
     * Handle start download event.
     */
    void onDownloadStart();

    /**
     * Handle success download.
     * The download value is given after a post-processing, notify with
     * {@code onPostDownload}.
     *
     * @param value The post-processed downloaded value.
     */
    void onDownloadSuccess( T value );

    /**
     * Handle failed download.
     */
    void onDownloadFailed();

    /**
     * Handle post processing after a download.
     */
    void onPostDownload();

    /**
     * Handle download end.
     */
    void onDownloadFinish();
}
