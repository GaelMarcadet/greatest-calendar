package com.example.agenda.activities.modules.display;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.agenda.R;
import com.example.agenda.activities.calendar.display.CalendarDisplayActivity;
import com.example.agenda.activities.events.display.EventDisplayActivity;
import com.example.agenda.activities.scholar_years.display.OnItemInteraction;
import com.example.agenda.activities.scholar_years.display.ScholarYearsDisplayActivity;
import com.example.agenda.core.buffer.Buffer;
import com.example.agenda.core.storage.Entity.Module;

import java.util.ArrayList;
import java.util.List;

public class ModuleDisplayActivity extends AppCompatActivity implements OnItemInteraction<Module> {

    private long year_id;

    private List<Module> modules;

    private RecyclerView recyclerView;

    private ModulesAdapter modulesAdapter;

    public ModuleDisplayActivity(){
        this.year_id = -1;
        this.modules = new ArrayList<>();
    }

    @Override
    public void onCreate(@Nullable  Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent year = getIntent();

        if(year != null){
            year_id = year.getLongExtra("year_id",-1);
            Log.i("NORMAL RUNNING", "Information found in Intent");
        }else{
            Log.i("ANORMAL RUNNING","EMPTY INTENT");
        }

        setContentView(R.layout.activity_scholar_details);

        startToolbar();
        setTitle(Buffer.getInstance().getScholarYearById(year_id).getTitle());
        this.modules = Buffer.getInstance().getModulesbyId(year_id);
        Log.i("MODULES ", modules.toString());
        // Creating views
        this.recyclerView = findViewById(R.id.modules_recycler_view);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        this.modulesAdapter = new ModulesAdapter(this.modules);
        this.modulesAdapter.addObserver(this);
        this.recyclerView.setAdapter(modulesAdapter);
    }

    private void startToolbar() {
        Toolbar toolbar = findViewById( R.id.tool_bar);
        this.setSupportActionBar( toolbar );

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate( R.menu.menu_module, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item) {
        int id = item.getItemId();

        if ( id == R.id.menu_module_back) {
            goToMainActivity();
        }else if( id == R.id.mode_calendar){
            goToCalendarActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    private void goToCalendarActivity() {
        Log.i("GOTO CALENDAR","Go to the calendar display");
        Intent intent = new Intent(this,CalendarDisplayActivity.class);
        startActivity(intent);
    }

    public void goToMainActivity(){
        Log.i("GOTO MAIN","Return in main activity");
        finish();
    }

    @Override
    public void onItemInteraction(Module module) {
        Log.i( "SCHOLAR_YEAR_INTERACT", "onItemInteraction: Starting "
                + module.getSummary() );
        Log.i( "SCHOLAR_YEAR_INTERACT", "Display in progress" );
        displayEvent( module);
    }

    private void displayEvent(Module module) {
        Log.d( "EVENT_DISP", "Displaying Module " + module.getSummary() + " id " + module.getId());
        Intent intent = new Intent(this, EventDisplayActivity.class);
        intent.putExtra("module_id",module.getId());
        startActivity(intent);
    }

}
