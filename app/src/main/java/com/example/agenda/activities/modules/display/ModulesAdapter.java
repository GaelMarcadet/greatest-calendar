package com.example.agenda.activities.modules.display;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.agenda.R;
import com.example.agenda.core.adapter.GenericViewHolder;
import com.example.agenda.core.adapter.ListAdapter;
import com.example.agenda.core.storage.Entity.Module;

import java.util.List;

public class ModulesAdapter extends ListAdapter<Module> {

    ModulesAdapter(List<Module> initialModules){
        super(initialModules);
    }

    @Override
    public View inflateView(ViewGroup parent, LayoutInflater layoutInflater) {

        return layoutInflater.inflate(R.layout.fragment_module,parent,false);
    }

    @Override
    public GenericViewHolder createViewHolder(View view) {
        return new ModuleViewHolder( view );
    }
}
