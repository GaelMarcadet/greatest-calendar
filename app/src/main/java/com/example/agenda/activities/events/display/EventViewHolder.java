package com.example.agenda.activities.events.display;

import android.annotation.SuppressLint;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.example.agenda.R;
import com.example.agenda.core.adapter.GenericViewHolder;
import com.example.agenda.core.storage.Entity.Event;

public class EventViewHolder extends GenericViewHolder<Event> {

    private TextView begin_day;

    private TextView start_end;


    EventViewHolder(@NonNull View itemView) {
        super(itemView);
        this.begin_day = itemView.findViewById(R.id.begin_day);
        this.start_end = itemView.findViewById(R.id.event_start_end);
    }



    @SuppressLint("SetTextI18n")
    @Override
    public void updateViewHolder(Event item) {
        String day = (String) DateFormat.format("dd/MM/yyyy",item.getStartAt());
        this.begin_day.setText(day);

        String start = (String) DateFormat.format("HH:mm",item.getStartAt());
        String end = (String) DateFormat.format("HH:mm",item.getEndAt());
        String date = String.format(getContext().getResources().getString(R.string.duration_event),start,end);
        this.start_end.setText(date);
    }

}
