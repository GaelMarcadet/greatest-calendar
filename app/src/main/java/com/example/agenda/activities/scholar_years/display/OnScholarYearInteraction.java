package com.example.agenda.activities.scholar_years.display;

import com.example.agenda.core.storage.Entity.ScholarYear;

@FunctionalInterface
public interface OnScholarYearInteraction  extends OnItemInteraction< ScholarYear > {
}
