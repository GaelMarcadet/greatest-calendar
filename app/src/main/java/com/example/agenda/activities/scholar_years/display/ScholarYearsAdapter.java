package com.example.agenda.activities.scholar_years.display;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.agenda.R;
import com.example.agenda.core.adapter.ListAdapter;
import com.example.agenda.core.adapter.GenericViewHolder;
import com.example.agenda.core.storage.Entity.ScholarYear;

import java.util.List;

/**
 * Specialized adapter for scholar years.
 */
public class ScholarYearsAdapter extends ListAdapter< ScholarYear > {

    public ScholarYearsAdapter( List<ScholarYear> initialScholarYears ) {
        super( initialScholarYears );
    }

    @Override
    public View inflateView( ViewGroup parent, LayoutInflater layoutInflater ) {
        return layoutInflater.inflate( R.layout.fragment_scholar_year, parent, false);
    }

    @Override
    public GenericViewHolder createViewHolder( View view ) {
        return new ScholarYearViewHolder( view );
    }

}
