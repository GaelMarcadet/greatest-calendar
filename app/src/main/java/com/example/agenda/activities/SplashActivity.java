package com.example.agenda.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.agenda.R;
import com.example.agenda.activities.scholar_years.display.ScholarYearsDisplayActivity;
import com.example.agenda.core.buffer.Buffer;
import com.example.agenda.core.buffer.BufferLoader;
import com.example.agenda.core.buffer.OnDatabaseInteraction;
import com.example.agenda.core.storage.Facade.Storage;

public class SplashActivity extends AppCompatActivity {

    private static long SPLASH_SCREEN_SLEEP = 1500;
    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_splash );
    }

    @Override
    protected void onStart() {
        super.onStart();

        BufferLoader loader = new BufferLoader( new Storage( this ) );
        loader.setOnDatabaseInteraction( new OnDatabaseInteraction() {
            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {
                startApplication();
            }
        } );
        loader.execute();
    }

    private void startApplication() {
        Intent intent = new Intent( this, ScholarYearsDisplayActivity.class );
        intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
        startActivity( intent );
    }

}
