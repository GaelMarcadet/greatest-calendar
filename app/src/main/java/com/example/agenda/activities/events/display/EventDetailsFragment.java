package com.example.agenda.activities.events.display;


import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.example.agenda.R;
import com.example.agenda.core.storage.Entity.Event;

public class EventDetailsFragment extends Fragment {

        private TextView start_end;

        private TextView summary;

        private TextView location;

        private TextView description;

        public EventDetailsFragment() { }

        @Override
        public void onCreate(Bundle savedInstanceState) {

                super.onCreate(savedInstanceState);

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

                View details =  inflater.inflate(R.layout.fragment_event_details, container, false);
                summary =  details.findViewById(R.id.summary);
                location = details.findViewById(R.id.location);
                description = details.findViewById(R.id.description);
                start_end = details.findViewById(R.id.start_end);
                return details;
        }


        @Override
        public void onAttach(@NonNull Context context) {
                super.onAttach(context);
        }

        @Override
        public void onDetach() {
                super.onDetach();
        }

        public void update(Event event)
        {
                Log.d("EVENT DETAILS FRAGMENT","Update called");
                if (event == null)
                {
                        summary.setText("");
                        location.setText("");
                        description.setText("");
                        start_end.setText("");
                        return;
                }
                summary.setText(String.format(getContext().getResources().getString(R.string.details_summary),event.getSummary()));
                location.setText(String.format(getContext().getResources().getString(R.string.details_location),event.getLocation()));
                description.setText(String.format(getContext().getResources().getString(R.string.details_description),event.getDescription()));
                String start = (String) DateFormat.format("HH:mm",event.getStartAt());
                String end = (String) DateFormat.format("HH:mm",event.getEndAt());
                String date = String.format(getContext().getResources().getString(R.string.duration_event),start,end);
                start_end.setText(date);

        }

}


